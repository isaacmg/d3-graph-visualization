//import * as d3 from "d3";

export default function Graph( ){
  // w: weight of the svg element
  this.w = 600;
  // h: height of the svg element
  this.h = 500;
  // border, default value: true, defined in options
  this.border = true;
  // sociotype: 0: Preference, 1: Perception Preference, 2: Perception Rejection, 3: Rejection. 0 by default.
  this.socioType = 0;
  // Store the graph
  this.graph;
  // Store the svg element
  this.svg;
  // Store the g element
  this.g;
  // Hash of the links to verify that exists in a fast way
  this.linkedHash = {};
  // Array ordenat per la key "Preference", ....
  this.sortedLinksByKey;
  // Array amb els nodes que no tenen connexions
  this.noLinkNodes = new Array(4);

  // 3 Maps to translate socioType to idSocioType, idSocioType to socioType, idColor to colorFunction
  this.toSocioType = new Map();
  this.toSocioType.set("preference",0).set("perception_preference",1).set("perception_rejection",2).set("rejection",3);
  this.fromSocioType = new Map();
  this.fromSocioType.set(0,"preference").set(1,"perception_preference").set(2,"perception_rejection").set(3,"rejection");
  this.fromColor = new Map();
  this.fromColor.set(0,"colorDegree").set(1,"colorBetweenNess").set(2,"colorCloseNess").set(3,"colorSex");

  // Array to store the max in the min centralities (Betweeness, Degree, Closeness) of the 4 socioTypes.
  this.centr = {
        betNess: [{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()}],
        deg: [{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()}],
        cloNess:[{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()}]
      };

  // Variable that stores the links of the graph as a Array
  this.links = [];

  // Store the color id and the color Function to change the node colors.
  this.color;
  this.colorFunction = "colorDegree";

  // Store the svg elements needed for create and changing the svg.
  this.link;
  this.node;
  this.markersRadius;
  this.markerSVG;
  this.circle;
  this.diana;
  this.simulation;
  // Boolean variable to store the autoCenter property.
  this.center = true;
  this.simulation.center = true;


  // Force variables
  this.forces = {
    forceStrength: -85.6,
    chargeDistanceMin: 30.4,
    chargeDistanceMax: 207.8,
    forceCollide: 1,
    forceLinks: 50,
    forceLinkStrength: 0.01
  };

  // Forces availables.
  this.availables = {
    "forceManyBody": 1,
    "forceLink": 1,
    "forceCenter": 1,
    "forceCollide": 1
  };

  // Is force active?
  this.isForceActive = true;
}



// Define the functions in prototype to share them with the subclasses.
Graph.prototype = {

  //Function that change between enable and disable the autoCenter in the graph.
  autoCenter: function(b){
    this.simulation.center = b;
    this.center = b;
  },

  // Function that switch the activity of the forces
  forceSwitch: function(){
    this.isForceActive = ! this.isForceActive;
    console.log(this.isForceActive);
  },
  // Function to change the availability of the forces
  setAvailabilityForces: function(availables){
    Object.keys(availables).forEach(key => {
      this.availables[key] = availables[key] ? 1 : 0;
    });
  },
  // Function to change the forces of the graph
  setForces: function(forces){
    forces = forces || {};
    this.forces.forceStrength = forces.forceStrength || -85.6;
    this.forces.distanceMin = forces.distanceMin || 30.4;
    this.forces.distanceMax = forces.distanceMax || 207.8;
    this.forces.forceCollide = forces.forceCollide || 1;
    this.forces.forceLinks = forces.forceLinks || 50;
    this.forces.forceLinkStrength = forces.forceLinkStrength || 0.01;
  },
  restartForces: function (){
    this.forces.forceStrength = -85.6;
    this.forces.distanceMin = 30.4;
    this.forces.distanceMax = 207.8;
    this.forces.forceCollide = 1;
    this.forces.forceLinks = 50;
    this.forces.forceLinkStrength = 0.01;
  },

  updateForces: function(){
    /*this.simulation.force("charge")
        .strength(this.forces.forceStrength)
        .distanceMin(this.forces.chargeDistanceMin)
        .distanceMax(this.forces.chargeDistanceMax);
    this.simulation.force("collide")
            .strength(this.forceCollide);
    this.simulation.force("link")
                   .distance(this.forces.forceLinks);
    this.simulation.alpha(1).restart();*/
    this.restart();
  },

  // Function to manage the optios parameter that receives on creation.
  // set options to the options supplied or an empty object if none are provided
  manageOptions: function( options ){
    options = options || {};
    this.border = options.border || true;
    this.w = options.w || 500;
    this.h = options.h || 500;
  },

  // Function that creates the svg elements to stores the graph in the id element of the html.
  createSVG: function( id ){
    //this.svg = d3.select("#"+id).append("svg").attr("width", this.w).attr("height", this.h);
    this.svg = d3.select("#"+id).append("svg").attr("viewBox", "0 0 "+ this.w + " "+ this.h).attr("preserveAspectRatio", "xMidYMid meet").classed("svg-content", true);
    this.svg.attr("style", "outline: thin solid grey;");
    this.g = this.svg.append("g").attr("transform", `translate(${this.w / 2},${this.h / 2})`).attr("id","centerGraph");
    this.diana = this.g.append("g").attr("id","dianaGraph");
    this.link = this.g.append("g").attr("stroke", "#888").attr("id","edgesGraph").selectAll("line");
    this.node = this.g.append("g").attr("id","nodesGraph").selectAll(".node");
  },

  // Function that every subclass redefines to execute particular things in every subclass.
  addProperties: function(){},

  // Function that stores the graph.
  setGraph: function( json ){
    this.graph = json;
  },

  // Function that creates two structures to manage the links.
  // First: linkedHash creates the hash to detect if one link exists fast.
  // Second: sortedLinksByKey sorts the link in a array by socioType.
  calcEdges: function(){
    let edges = [];

    this.graph.links.forEach((e) => {
      let sourceNode = this.graph.nodes.filter(function(n) {return n.id === e.source;})[0];
      let targetNode = this.graph.nodes.filter(function(n) { return n.id === e.target; })[0];
      edges.push({source: sourceNode, target: targetNode, weight: e.weight, key: e.key, keyValue: this.toSocioType.get(e.key)});
      this.linkedHash[`${e.source},${e.target},${this.toSocioType.get(e.key)}`] = true;
    });

    this.sortedLinksByKey = d3.nest()
        .key(function(d) { return d.keyValue; }).sortKeys(d3.ascending)
        .entries(edges);
  },

  // Function that iterates the nodes for:
  // 1. calculate the max and min centralities to choose the correct weight of colors by every socioType.
  // 2. calculate the number of connections of every node.
  calcNodes: function(){
    let radis = [];
    for (let i = 0; i < 4; i++)
      this.noLinkNodes[i] = {};



    this.graph.nodes.forEach((e) => {
      let con = [];
      for (let i =0;i < 4;i++) {
        let aux = `${this.fromSocioType.get(i)}_betweenness`;
        if (this.centr.betNess[i].max < e[aux]) this.centr.betNess[i].max = e[aux];
        if (this.centr.betNess[i].min > e[aux]) this.centr.betNess[i].min = e[aux];
        aux = `${this.fromSocioType.get(i)}_degree`;
        if (this.centr.deg[i].max < e[aux]) this.centr.deg[i].max = e[aux];
        if (this.centr.deg[i].min > e[aux]) this.centr.deg[i].min = e[aux];
        aux = `${this.fromSocioType.get(i)}_closeness`;
        if (this.centr.cloNess[i].max < e[aux]) this.centr.cloNess[i].max = e[aux];
        if (this.centr.cloNess[i].min > e[aux]) this.centr.cloNess[i].min = e[aux];

        let connections = this.numberConnectionsSocioType(e,i);
        radis.push(connections.source);
        // This condition identifies if there are nodes without links
        if (connections.source == 0 && connections.target == 0) this.noLinkNodes[i][e.id] = e.node_id;
        con.push(connections.target);
      }
      // Stroring the con array to the node in the json graph.nodes
      e.con = con;

    });
    this.markersRadius = Array.from(new Set(radis));

  },
  radiusChange: function(radius){
    return radius+6;
  },
  // Function that test if value is undefined
  isUndefined: function(value){
      return value === undefined;
  },

  // Function that is used in calcNodes to calculate:
  // 1. the links that receives a node in one socioType.
  // 2. the links that gives one node in one socioType.
  numberConnectionsSocioType: function(a, m) {
    let t = 0;
    let s = 0;
    if (!this.isUndefined(this.sortedLinksByKey[m]))
      this.sortedLinksByKey[m].values.forEach(function(e) {
          if ((e.target.id == a.id)) t++;
          if ((e.source.id == a.id)) s++;
      });
    return {
      'source': s,
      'target': t
    };
  },

  // Function that is redefined in subclasses. Every subclass has different markers.
  addMarkers: function(){
  },

  // Function that starts the simulation calling startSimulation and restart.
  simulation: function(){
    this.startSimulation();
    this.restart();
  },

  // Function that stores in the links variable the links of the socioType choosen.
  startSimulation: function(){
    if (!this.isUndefined(this.sortedLinksByKey[this.socioType]))
        this.links = this.sortedLinksByKey[this.socioType].values;
  },

  // Function that loops when there are changes in the graph to change nodes and links positions in the svg.
  ticked: function() {
    let that = this;
    this.link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        //.attr("x2", function(d) { return d.target.x; })
        //.attr("y2", function(d) { return d.target.y; });
        .attr("x2", function(d) {
          var x1 = d.source.x,y1 = d.source.y,x2 = d.target.x,y2 = d.target.y,angle = Math.atan2(y2 - y1, x2 - x1);
          return x2 - Math.cos(angle) * (d.target.con[that.socioType]+6);
         })
        .attr("y2", function(d) {
          var x1 = d.source.x,y1 = d.source.y,x2 = d.target.x,y2 = d.target.y,angle = Math.atan2(y2 - y1, x2 - x1);
          return y2 - Math.sin(angle) * (d.target.con[that.socioType]+6);
         });

    this.node.attr("transform", d => `translate(${d.x}, ${d.y})`);
  },
  // Function that executes when simulation is finished.
 finishedSim: function(){
   console.log(this);
   if (this.center){
     //let positions = [];
     // This is the way to access the x and y values of the translate and scale transform attribute in g tag "centerGraph".
     //let centerGraph = d3.select("#centerGraph");
     //let transform = centerGraph.node().transform.baseVal
     //let scale = 1;
     //if(transform.numberOfItems == 2) scale = transform.getItem(1).matrix.a;

     // x value is in e attribute and y value is in f attribute.
     //let translate = transform.getItem(0).matrix;
     let maxX = Math.max();
     let maxY = Math.max();
     let minX = Math.min();
     let minY = Math.min();

     this.node.each(function(d){
       if (maxX < d.x) maxX = d.x;
       if (maxY < d.y) maxY = d.y;
       if (minX > d.x) minX = d.x;
       if (minY > d.y) minY = d.y;

     });

     let scaleArray = [1,1,1,1];

     if ((maxX+this.w/2) > this.w) scaleArray[0]=this.w/(maxX+this.w/2+50);
     if ((maxY+this.h/2) > this.h) scaleArray[1]=this.h/(maxY+this.h/2+50);
     if ((minX+this.w/2) < 0) scaleArray[2]=this.w/(Math.abs(minX)+this.w/2+this.w+50);
     if ((maxY+this.h/2) < 0) scaleArray[3]=this.h/(Math.abs(minY)+this.h/2+this.h+50);
     let minScale = Math.min(...scaleArray);

     let centerX = (maxX+minX)/2;
     let centerY = (maxY+minY)/2;

     d3.select('svg')
  	  .transition()
  	  .call(this.zoom.translateTo, 0.5 * this.w +centerX , 0.5 * this.h+centerY)
      .transition().call(this.zoom.scaleTo, minScale);

  }
 },
  // Function that verifies if 2 nodes are connected using the hash created before.
  isConnected: function(a, b) {
      return this.linkedHash[`${a.id},${b.id},${this.socioType}`] || this.linkedHash[`${b.id},${a.id},${this.socioType}`] || a.id == b.id;
  },

  // Function that highlight the nodes connected to a node.
  set_highlight: function(d){
    var that = this;
    this.circle.attr("stroke", function(o) {
              return that.isConnected(d, o) ? "blue" : "grey";
    });
    this.circle.attr("stroke-width", function(o) {
      return that.isConnected(d, o) ? "2" : "1";
    });
    this.circle.attr("opacity", function(o) {
      return that.isConnected(d, o) ? "1" : "0.5";
    });

    this.text.style("font-weight", function(o) {
      return that.isConnected(d, o) ? "bold" : "normal";
    });
    this.text.attr("opacity", function(o) {
      return that.isConnected(d, o) ? 1 : 0.5;
    });

    this.link.style("stroke", function(o) {
      return o.source.index == d.index || o.target.index == d.index ? "blue" : "#eee";
    });
    this.link.attr("opacity", function(o) {
      return o.source.index == d.index || o.target.index == d.index ? 1 : 0;
    });

    this.markerSVG.attr("fill", "blue");
  },

  // Function that puts all the nodes in the same color border when the node is exit.
  exit_highlight: function(){
    this.circle.attr("stroke", "grey").attr("stroke-width", 1).attr("opacity",1);
    this.text.style("font-weight", "normal").attr("opacity",1);
    this.link.style("stroke", "grey").attr("opacity",1);
    this.markerSVG.attr("fill", "grey");
  },

  // Function to color the nodes depending on the number of connections
  colorNumConnections: function(){
    let color = d3.scaleLinear()
      .domain([0,7])
      .range(['blue','white']);

    let colorRed = d3.scaleLinear()
      .domain([0,7])
      .range(['red','white']);

    this.circle = this.circle
                .attr("fill", (d) => { return ( (this.socioType == 0 || this.socioType == 1) ? color(this.numberConnections(d)) : colorRed(this.numberConnections(d))); })
                .attr("stroke", "grey");
  },

  // Function to color the nodes depending on the Betweeness centrality
  colorBetweenNess: function(){
    let aux = `${this.fromSocioType.get(this.socioType)}_betweenness`;

    let colorBet = d3.scaleLinear()
                    .domain([this.centr.betNess[this.socioType].min,this.centr.betNess[this.socioType].max])
                    .range(['DarkGreen','white'])
                    .unknown("DarkGreen");
    this.circle = this.circle
                .attr("fill", function(d) { return  colorBet(d[aux]);})
                .attr("stroke", "grey");
  },

  // Function to color the nodes depending on the Degree centrality
  colorDegree: function(){
    let aux = `${this.fromSocioType.get(this.socioType)}_degree`;
    let colorDeg = d3.scaleLinear()
                    .domain([this.centr.deg[this.socioType].min,this.centr.deg[this.socioType].max])
                    .range(['blue','white'])
                    .unknown("blue");
    this.circle = this.circle
                .attr("fill", function(d) { return  colorDeg(d[aux]);})
                .attr("stroke", "grey");
  },

  // Function to color the nodes depending on the Closeness centrality
  colorCloseNess: function(){
    let aux = `${this.fromSocioType.get(this.socioType)}_closeness`;
    let colorClo = d3.scaleLinear()
                    .domain([this.centr.cloNess[this.socioType].min,this.centr.cloNess[this.socioType].max])
                    .range(['orange','white'])
                    .unknown("orange");
    this.circle = this.circle
                .attr("fill", function(d) { return  colorClo(d[aux]);})
                .attr("stroke", "grey");
  },

  // Function to color the nodes depending on the sex of the nodes.
  colorSex: function(){
    this.circle = this.circle
                .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
                .attr("stroke", "black");
  },


  // Function that calculate the links of a node in the current socioType.
  numberConnections: function(a) {
    return this.numberConnectionsSocioType(a,this.socioType).target;
  },

  // Function that calculates the number of links of the nodes and return as a array.
  linksPerNode: function(a){
    let aux = [];
    let linksSocioType = [];
    if (!this.isUndefined(a[this.socioType])) linksSocioType = a[this.socioType].values;
    this.graph.nodes.forEach(function (node) {
      aux[node.node_id] = 0;
    });
    linksSocioType.forEach(function (link) {
      aux[link.target.node_id] = aux[link.target.node_id] + 1;
    });

    return aux;
  },

  // Function that find the max value of an array
  maxLinksPerNode: function(a){
    let aux = -1;
    for (var key in a){
      if (a[key] > aux) aux = a[key];
    }
    return aux;
  },
  // Function that find the min value of an array
  minLinksPerNode: function(a){
    let aux = this.graph.nodes.length*2;
    for (var key in a){
      if (a[key] < aux) aux = a[key];
    }
    return aux;
  },
  dragstarted:  function(d){
    if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  },
  dragged: function(d){
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  },
  dragended: function(d){
    if (!d3.event.active) this.simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }

};
