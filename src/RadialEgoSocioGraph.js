//import * as d3 from "d3";
import Graph from "./Graph.js";

export default function RadialEgoSocioGraph(options){
  this.manageOptions(options);
  this.member = options.member;
  this.max = 0;
  this.toColorArrow = new Map();
  this.toColorArrow.set("preference","blue").set("perception_preference","blue").set("perception_rejection","red").set("rejection","red");
  this.toDashArrow = new Map();
  this.toDashArrow.set("preference","0").set("perception_preference","5,5").set("perception_rejection","5,5").set("rejection","0");

  this.addMarkers = function(){
    let markers = [];
    for (var valor of this.toColorArrow.values()) {
      markers.push(valor);
    }
    this.markerSVG = this.svg.append("defs").selectAll("marker")
       .data(markers)
       .join("marker")
         .attr("id", function(d){return d;})
         .attr("viewBox", "0 -5 10 10")
         .attr("refX", 15)
         .attr("refY", -0.5)
         .attr("markerWidth", 6)
         .attr("markerHeight", 6)
         .attr("orient", "auto")
       .append("path")
         .attr("stroke-width", 3)
         .attr("fill",  function(d){return d;})
         .attr("d", "M0,-3L10,0L0,3");

  }
  this.addProperties = function(){
    d3.select("#edgesGraph").remove();
    this.link = d3.select("#centerGraph").append("g").attr("id","edgesGraph").attr("fill", "none").attr("stroke", "#888").selectAll("path");
    d3.select("#nodesGraph").remove();
    this.node = d3.select("#centerGraph").append("g").attr("id","nodesGraph").selectAll(".node");
  }
  this.allLinksMember = function(member){
    let newLinks = [];
    for(var i =0; i < 4; i++){
      for(var j =0; j < this.sortedLinksByKey[i].values.length; j++){
        if (this.sortedLinksByKey[i].values[j].source.id == member.id || this.sortedLinksByKey[i].values[j].target.id == member.id)
          newLinks.push(this.sortedLinksByKey[i].values[j]);
      }
    }
    return newLinks;
  }
  this.startSimulation = function(){
    if (!this.isUndefined(this.sortedLinksByKey[0]))
        this.links = this.allLinksMember(this.memberOnNodes(this.member));
    this.distanceNodes(this.memberOnNodes(this.member));
  }
  this.memberOnNodes = function(memberId){
    var i = 0;
    var result = this.graph.nodes[0];
    while (this.graph.nodes[i].id != memberId && i < this.graph.nodes.length){
      i++;
    }
    if (i < this.graph.nodes.length) result = this.graph.nodes[i];
    return result;
  },

  this.restart = function(){
    var that = this;
    let colorCircle = d3.scaleLinear()
    .domain([1,8])
    .range(["#3f51b5","white"]);

    this.node = this.node.data(this.graph.nodes).join("g").attr("class","node");

    // Afegim els cercles concentrics
    for (let i = 8; i >= 1 ; i--){
      this.diana.append("circle")
          .attr("r",i*(200/8))
          .attr("stroke","brown")
          .attr("stroke-opacity",0.5)
          .attr("fill",colorCircle(i));
    }

    this.circle = this.node.append("circle")
         .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
         .attr("stroke", "grey").attr("stroke-width", 1.5)
         .call(function(node) { node.transition().attr("r", 8); })

         this.circle.call(d3.drag()
                  .on("start", function(d){ that.dragstarted(d); })
                  .on("drag", function(d){ that.dragged(d); })
                  .on("end", function(d){ that.dragended(d); }))


     this.text = this.node.append("text")
         .text(function(d) { return d.node_id; })
      //   .text(function(d) { return d.node_id +":"+d.id; })
         .style("fill","#000")
         .style("font-size","10px")
         .attr("x", 10)
         .attr("y", 0)

         this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; })
                     .enter().append("path").attr("stroke-width", 2)
                     .attr("stroke-dasharray",  function(d) { return that.toDashArrow.get(d.key); })
                     .attr("marker-end", function(d) { return "url(#"+that.toColorArrow.get(d.key)+")" })
                     .attr("stroke", function(d) { return that.toColorArrow.get(d.key); })
                     .call(function(link) { link.transition().attr("stroke-opacity", 1); });

      this.simulation = d3.forceSimulation(this.graph.nodes)
    //      .force('link', d3.forceLink().links(this.links))
          .force('charge', d3.forceManyBody().strength(-3))
      //    .force("charge", d3.forceCollide().radius(15))
          .force("r", d3.forceRadial(function(d) { return -(d.distance-5)*(200/8); }).strength(0.7))
          .alpha(1).alphaMin(0.005).velocityDecay(0.1)
          .on("tick", this.ticked2);

      // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
      this.simulation.node = this.node;
      this.simulation.link = this.link;

  },
  this.ticked2 = function(){
    this.node.attr("transform", d => `translate(${d.x}, ${d.y})`);

    this.link.attr("d", function(d) {
      const r = Math.hypot(d.target.x*5 - d.source.x*5, d.target.y - d.source.y);
      return "M"+d.source.x+","+d.source.y+ " A"+r+","+r+" 0 0,1 "+d.target.x+","+d.target.y;
    });


  },

  this.distanceNodes = function(src) {

    var that = this;
    this.graph.nodes.forEach(function (d) {
      d.distance = 0;
        for (let i= 0; i < 4 ; i++){
          if (that.linkedHash[`${d.id},${src.id},${i}`] || that.linkedHash[`${src.id},${d.id},${i}`]) {
            if (i == 0 || i == 1){
              d.distance++;
            } else {
              d.distance--;
            }
          }
        }
    });
    src.distance = 5;
  }
}
RadialEgoSocioGraph.prototype = new Graph();
