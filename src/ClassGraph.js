//import * as d3 from "d3";
import Graph from "./Graph.js";

// Subclass to generate forced group graph.
export default function ClassGraph(options){
  this.groupNum = options.groupNum || 2;
  this.classe = [];
  this.xCenter = [];
  this.yCenter = [];

  this.manageOptions(options);

  this.addProperties = function(){
      this.svg = this.svg.style("cursor","move");
  }

  // Function that starts the simulation calling startSimulation and restart.
  this.simulation = function(){
    this.startSimulation();
    this.algorithmClass();
    this.startSimulation();
    this.restart();
  }

  this.addMarkers = function(){
    // Defining Arrows
    this.markerSVG = this.svg.append("defs").selectAll("marker")
       .data(this.markersRadius)
       .join("marker")
         .attr("id", function(d){return `arrow-${(d*2 + 8)}`;})
         .attr("viewBox", "0 -5 10 10")
         .attr("refX", function(d){return d + 20;})
         .attr("refY", 0)
         .attr("markerWidth", 12)
         .attr("markerHeight", 8)
         .attr("orient", "auto")
         .append("path")
         .attr("fill", "grey")
         .attr("d", "M0,-2L20,0L0,2");
  }
  this.restart = function(){

        // Apply the general update pattern to the nodes.
        this.node = this.node.data(this.graph.nodes, function(d) { return d.node_id;});
        d3.selectAll("circle").remove();
        d3.selectAll("text").remove();

        this.node = this.node.join("g").attr("class","node");

        this.node.exit().transition()
            .attr("r", 0)
            .remove();

        this.circle = this.node.append("circle")
                 .attr("stroke-width", 1).attr("class", "nodesCircles")
                 .attr("node_id", function(d){return d.node_id; }).attr("id", function(d){return d.id; })
                 .call((node) => { node.transition().attr("r", (d) => { return this.radius(d); }); });

        var that = this;
        this.circle.call(d3.drag()
                 .on("start", function(d){ that.dragstarted(d); })
                 .on("drag", function(d){ that.dragged(d); })
                 .on("end", function(d){ that.dragended(d); }))
                 .on("dblclick", function(d){ that.dblclickAction(d); })
                 .on("mouseover", function(d){ that.set_highlight(d); })
                 .on("mouseout",  function(){ that.exit_highlight(); });

        // Es concreta el color dels nodes depenen de la variable colorFunction
        this[this.colorFunction]();

        this.text = this.node.append("text")
            .text(function(d) { return d.node_id; })
            .style("fill","#000")
            .style("font-size", "10px")
            .attr("x", 10)
            .attr("y", 0);

        this.node.merge(this.node);

        // Apply the general update pattern to the links.
        this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; });
        // Keep the exiting links connected to the moving remaining nodes.
        // When no links and no nodes, this.link is Array. Normally it is an Object.
        if (!Array.isArray(this.link)){
            this.link.exit().transition()
                .attr("stroke-opacity", 0)
                .attrTween("x1", function(d) { return function() { return d.source.x; }; })
                .attrTween("x2", function(d) { return function() { return d.target.x; }; })
                .attrTween("y1", function(d) { return function() { return d.source.y; }; })
                .attrTween("y2", function(d) { return function() { return d.target.y; }; })
                .remove();
            this.link = this.link.join("line").attr("stroke-width", 1.5)
                .call((link) => { link.transition().attr("stroke-opacity", 1); })
                .attr("marker-end", (d) => {return `url(${new URL("#arrow-"+this.radius(d.target), location)})`;})
                .merge(this.link);

        }


/*        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("charge", d3.forceManyBody().strength(-85.6).distanceMin(30.4).distanceMax(207.8))
            .force("collide", d3.forceCollide().strength(80).radius(function(d) { return d.r; }))
            .force("link", d3.forceLink(this.links).id(function(d) { return d.id; }).distance(50))
                .force("center", d3.forceCenter()).alphaMin(0.01)
                .on("tick", this.ticked);

        // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
        this.simulation.node = this.node;
        this.simulation.link = this.link;

        // Update and restart the simulation.
        this.simulation.nodes(this.graph.nodes);
        this.simulation.force("link").links(this.links);
        this.simulation.alpha(1).restart();
*/
console.log("Adios");
      this.simulation = d3.forceSimulation(this.graph.nodes)
         .force('x', d3.forceX().x(function(d) {
           return that.xCenter[d.categoryX];
         }))
         .force('y', d3.forceY().y(function(d) {
           return that.yCenter[d.categoryY];
         }))
         .force('collision', d3.forceCollide().radius(30))
         .alphaMin(0.0001).alphaDecay(0.002).velocityDecay(0.2)
         .on('tick', that.ticked);

         // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
         this.simulation.node = this.node;
         this.simulation.link = this.link;

  }
  this.algorithmClass = function(){
    // Possibles groups (k combinatorics)
    var possiblesGroups = this.k_combinations(this.nodeIdsGraph(), this.groupNum);
    var that = this;

    // Scoring every group
    var possibleGroupsWithPoints = [];
    possiblesGroups.forEach(function(group){
      var points = that.pointsGroup(group);
      possibleGroupsWithPoints.push({
        group: group,
        points: points
      });
    });

    // Order by Scoring
    possibleGroupsWithPoints.sort(function(a, b){return  b.points-a.points});

    // Create Classes
    var classNumber = this.graph.nodes.length;
    var notFinish = true;
    possibleGroupsWithPoints.every(function(grup){
      // Si la classe està plena o quasi plena
      if (that.classe.length > (classNumber-that.groupNum) ) notFinish = false;

      // Comprobem que no hi hagi cap element a la classe
      var trobat = false;

      grup.group.forEach(function(x){
        if (that.classe.indexOf(x) != -1) trobat = true;
      });

      // Si no hi ha cap element, l'insertem a la classe
      if (!trobat)
        that.classe = that.classe.concat(grup.group);
      return notFinish;
    });

    // Acabar d'omplir
    if (this.classe.length < classNumber){
      var nodeIds = this.nodeIdsGraph();
      this.classe.forEach(function(el){
        var aux = nodeIds.indexOf(el);
        if (aux != -1)nodeIds.splice(aux, 1);
      });
      this.classe = this.classe.concat(nodeIds);
    }

    console.log(this.classe);

    // Creating the subclasses for simulation
    var numGroups = Math.ceil(classNumber / this.groupNum);

    for (var i = 0; i < numGroups; i ++){
      this.xCenter.push(i*(this.w/numGroups)-this.w/2+60);
    }
    this.yCenter = [-this.h/4,this.h/4];

    this.graph.nodes.forEach(function(node){
      node.categoryX = Math.floor(that.classe.indexOf(node.id)/that.groupNum)*2%numGroups;
    });
    this.graph.nodes.forEach(function(node){
      node.categoryY = Math.floor(that.classe.indexOf(node.id)/(that.graph.nodes.length/2));
    });

    // Erase extra links
    // Create groups
    var groups = [];
    for(var j= 0;j<this.classe.length;j=j+this.groupNum){
      groups.push(this.classe.slice(j,j+this.groupNum));
    }

    // Copia de seguretat
    this.graph.links2 = this.graph.links.slice();

    for(var k = 0; k <this.graph.links.length;k++){
      var link = this.graph.links[k];
      if (Math.floor(this.classe.indexOf(link.source)/this.groupNum) != Math.floor(this.classe.indexOf(link.target)/this.groupNum)) {
        this.graph.links.splice(k,1);
        k--;
      }
    }
    console.log(this.graph);

  // Transformem els links en una estructura ordenada pel tipus de sociograma (preference, ....)
  // Creem un hash per saber si dos nodes estan connectats
  let edges = [];

  this.graph.links.forEach((e) => {
    let sourceNode = this.graph.nodes.filter(function(n) {return n.id === e.source;})[0];
    let targetNode = this.graph.nodes.filter(function(n) { return n.id === e.target; })[0];
    edges.push({source: sourceNode, target: targetNode, weight: e.weight, key: e.key, keyValue: this.toSocioType.get(e.key)});
    this.linkedHash[`${e.source},${e.target},${this.toSocioType.get(e.key)}`] = true;
  });

  this.sortedLinksByKey = d3.nest()
      .key(function(d) { return d.keyValue; }).sortKeys(d3.ascending)
      .entries(edges);

  }

  this.radius = function(a){
    return this.numberConnections(a)*2 + 8;
  }

  this.dblclickAction = function(d){
    alert(`x:${d.x}, y:${d.y}, id:${d.id}, node_id:${d.node_id}`);
  }
  // Return as an Array ids all the graph.nodes
  this.nodeIdsGraph = function(){
    let result = [];
    this.graph.nodes.forEach(function(t){
      result.push(t.id);
    });
    return result;
  }
  this.pointsGroup = function(nodes){
      var point = 0;
      for (var i = 0; i < nodes.length; i++){
          for (var j = i+1; j < nodes.length; j++){
            if (i != j){
              if (this.linkedHash[nodes[i] + "," + nodes[j] + ",0" ] ) point++;
              if (this.linkedHash[nodes[j] + "," + nodes[i] + ",0" ] ) point++;
              if (this.linkedHash[nodes[i] + "," + nodes[j] + ",3" ] ) point--;
              if (this.linkedHash[nodes[j] + "," + nodes[i] + ",3" ] ) point--;
            }
          }
      }
      return point;
  }

  this.k_combinations = function(set, k) {
    var i, j, combs, head, tailcombs;
    // There is no way to take e.g. sets of 5 elements from
    // a set of 4.
    if (k > set.length || k <= 0) {
      return [];
    }

    // K-sized set has only one K-sized subset.
    if (k == set.length) {
      return [set];
    }
    // There is N 1-sized subsets in a N-sized set.
    if (k == 1) {
      combs = [];
      for (i = 0; i < set.length; i++) {
        combs.push([set[i]]);
      }
      return combs;
    }
    // Assert {1 < k < set.length}
    // Algorithm description:
    // To get k-combinations of a set, we want to join each element
    // with all (k-1)-combinations of the other elements. The set of
    // these k-sized sets would be the desired result. However, as we
    // represent sets with lists, we need to take duplicates into
    // account. To avoid producing duplicates and also unnecessary
    // computing, we use the following approach: each element i
    // divides the list into three: the preceding elements, the
    // current element i, and the subsequent elements. For the first
    // element, the list of preceding elements is empty. For element i,
    // we compute the (k-1)-computations of the subsequent elements,
    // join each with the element i, and store the joined to the set of
    // computed k-combinations. We do not need to take the preceding
    // elements into account, because they have already been the i:th
    // element so they are already computed and stored. When the length
    // of the subsequent list drops below (k-1), we cannot find any
    // (k-1)-combs, hence the upper limit for the iteration:
    combs = [];
    for (i = 0; i < set.length - k + 1; i++) {
      // head is a list that includes only our current element.
      head = set.slice(i, i + 1);
      // We take smaller combinations from the subsequent elements
      tailcombs = this.k_combinations(set.slice(i + 1), k - 1);
      // For each (k-1)-combination we join it with the current
      // and store it to the set of k-combinations.
      for (j = 0; j < tailcombs.length; j++) {
        combs.push(head.concat(tailcombs[j]));
      }
    }
    return combs;
  }

}
ClassGraph.prototype = new Graph();
